object fmVersionManager: TfmVersionManager
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #1052#1077#1085#1077#1076#1078#1077#1088' '#1074#1077#1088#1089#1080#1081
  ClientHeight = 252
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 634
    Height = 77
    Align = alTop
    ShowCaption = False
    TabOrder = 0
    object lblTittle: TLabel
      Left = 1
      Top = 1
      Width = 632
      Height = 75
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Caption = 
        #1055#1088#1080#1083#1086#1078#1077#1085#1080#1077' '#1085#1077' '#1084#1086#1078#1077#1090' '#1073#1099#1090#1100' '#1079#1072#1087#1091#1097#1077#1085#1086', '#1087#1086#1089#1082#1086#1083#1100#1082#1091' '#1077#1075#1086' '#1084#1086#1076#1091#1083#1080' '#1085#1077' '#1080#1084#1077#1102#1090 +
        ' '#1084#1080#1085#1080#1084#1072#1083#1100#1085#1086#1081' '#1089#1086#1074#1084#1077#1089#1090#1080#1084#1086#1089#1090#1080' '#1089' '#1090#1077#1082#1091#1097#1077#1081' '#1074#1077#1088#1089#1080#1077#1081' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085#1085#1086#1081' '#1089#1080 +
        #1089#1090#1077#1084#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 0
      ExplicitWidth = 472
      ExplicitHeight = 39
    end
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 200
    Width = 634
    Height = 52
    Align = alBottom
    ShowCaption = False
    TabOrder = 1
    object lblPostfix: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 514
      Height = 47
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 472
      ExplicitHeight = 39
    end
    object btnClose: TButton
      AlignWithMargins = True
      Left = 524
      Top = 4
      Width = 106
      Height = 44
      Align = alRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 1
      TabOrder = 0
    end
  end
  object tmrTerminate: TTimer
    Enabled = False
    OnTimer = tmrTerminateTimer
    Left = 216
    Top = 96
  end
end
