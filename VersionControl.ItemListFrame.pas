unit VersionControl.ItemListFrame;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  VirtualTrees,
  //System.ImageList,
  Vcl.ImgList,
  VersionControl.Classes;

type
  TfrVersionControl = class(TFrame)
    vt: TVirtualStringTree;
    ilCompatiblity: TImageList;
    procedure vtGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
      var Ghosted: Boolean; var ImageIndex: Integer);
    procedure vtGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: string);
    procedure vtFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure vtCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex;
      var Result: Integer);
  private
    function GetRealHeight: Integer;
    { Private declarations }
  public
    { Public declarations }
    procedure DisplayItems(Items: TVersionList);
    procedure DisplayAllItems(const AConnStr: string);
    procedure DisplayNotCompatibledItems(const AConnStr: string);

    property RealHeight: Integer read GetRealHeight;
  end;

implementation

uses
  uVirtualTreeViewHelper;

{$R *.dfm}

procedure TfrVersionControl.DisplayAllItems(const AConnStr: string);
var
  Items: TVersionList;
begin
  Items := TVersionList.Create;
  try
    TVersionControl.GetAllInstanceItemVersions(AConnStr, Items);
    DisplayItems(Items);
  finally
    Items.Free;
  end;
end;

procedure TfrVersionControl.DisplayItems(Items: TVersionList);
var
  i: Integer;
  Item: TVersionControlItem;
  b: Boolean;
begin
  vt.BeginUpdate;
  try
    vt.Clear;
    for i := 0 to Items.Count - 1 do
    begin
      Item := Items[i];
      vt.InsertNodeWithObject(nil, amAddChildLast, Item);
    end;
    b := Items.OwnsObjects;
    try
      Items.OwnsObjects := False;
      Items.Clear;
    finally
      Items.OwnsObjects := b;
    end;
  finally
    vt.EndUpdate;
  end;

  vt.DelayedFixHeader;
end;

procedure TfrVersionControl.DisplayNotCompatibledItems(const AConnStr: string);
var
  Items: TVersionList;
begin
  Items := TVersionList.Create;
  try
    TVersionControl.CheckInstanceCompatiblity(AConnStr, Items);
    DisplayItems(Items);
  finally
    Items.Free;
  end;
end;

function TfrVersionControl.GetRealHeight: Integer;
begin
  Result := Integer(vt.VisibleCount) * Integer(vt.DefaultNodeHeight) + vt.Header.Height + vt.BorderWidth;
end;

procedure TfrVersionControl.vtCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex;
  var Result: Integer);
var
  Item1, Item2: TVersionControlItem;
begin
  Item1 := Sender.GetObjectByNode<TVersionControlItem>(Node1);
  Item2 := Sender.GetObjectByNode<TVersionControlItem>(Node2);
  case Column of
    0:
      Result := AnsiCompareText(Item1.ModuleName, Item2.ModuleName);
    1:
      Result := Item1.ModuleCurrentVersion.CompareTo(Item2.ModuleCurrentVersion);
    2:
      Result := Item1.MinInstSupportedVersion.CompareTo(Item2.MinInstSupportedVersion);
    3:
      Result := Item1.MinDBSupportedVersion.CompareTo(Item2.MinDBSupportedVersion);
  end;
end;

procedure TfrVersionControl.vtFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  Sender.GetObjectByNode<TObject>(Node).Free;
end;

procedure TfrVersionControl.vtGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind;
  Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
var
  Item: TVersionControlItem;
begin
  Item := Sender.GetObjectByNode<TVersionControlItem>(Node);
  if (Column = 0) and (Kind <>ikOverlay) then
    ImageIndex := Integer(Item.IsCompatible);
end;

procedure TfrVersionControl.vtGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: string);
  function VersionTextFromItem(Item: TVersionControlItem; Ver: TVersion; const UnknownMainText: string): string;
  begin
    if Ver.FullVer <> 0 then
      Result := Ver.ToString
    else
      if not Item.MainProgramItem then
        Result := ''
      else
        Result := UnknownMainText;
  end;

var
  Item: TVersionControlItem;
begin
  Item := Sender.GetObjectByNode<TVersionControlItem>(Node);
  case Column of
    0:
      CellText := Item.ModuleName;
    1:
      begin
        if Item.ModuleCurrentVersion.FullVer <> 0 then
          CellText := Item.ModuleCurrentVersion.ToString
        else
          CellText := '�����������';

        if Item.IsCompatible = cvrBlocked then
          CellText := '(�������������) ' + CellText;
      end;
    2:
      CellText := VersionTextFromItem(Item, Item.MinInstSupportedVersion, '');
    3:
      CellText := VersionTextFromItem(Item, Item.MinDBSupportedVersion, '�� ��������������');
    4:
      CellText := VersionTextFromItem(Item, Item.ModuleActualVersion, '�� ��������������');
  end;
end;

end.
