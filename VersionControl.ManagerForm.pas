unit VersionControl.ManagerForm;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  VersionControl.Classes,
  VersionControl.ItemListFrame,
  Vcl.ExtCtrls,
  Vcl.StdCtrls;

type
  TfmVersionManager = class(TForm)
    pnlTop: TPanel;
    lblTittle: TLabel;
    pnlBottom: TPanel;
    lblPostfix: TLabel;
    btnClose: TButton;
    tmrTerminate: TTimer;
    procedure tmrTerminateTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    class var FInstance: TfmVersionManager;
    class var FFormCaption: string;
  private
    { Private declarations }
    Ffr: TfrVersionControl;
  strict private
    FTerminateOnIncompatible: Boolean;
    FTerminateTimeoutSec: integer;
    FConnStr: string;

    procedure DisplayNotActualVersions(const ConnStr: string; BelowStatus: TCheckVersionResult);
    procedure SetTerminateTimer(TerminateTimeoutSec: integer);

    procedure RegisterPeriodicallyCheck(const ConnStr: string; TerminateOnIncompatible: Boolean;
      TerminateTimeoutSec: integer);
    procedure IncompatibleCallback(Sender: TObject);

    procedure SetTittle(const Value: string);
    function GetTittle: string;
    function GetPostfix: string;
    procedure SetPostfix(const Value: string);

    function GetCloseButtonCaption: string;
    procedure SetCloseButtonCaption(const Value: string);

    constructor Create; reintroduce;
    class destructor Destroy;

    procedure SetUIText(CheckResult: TCheckVersionResult; TerminateOnIncompatible: Boolean;
      TerminateTimeoutSec: integer);

    property Tittle: string read GetTittle write SetTittle;
    property Postfix: string read GetPostfix write SetPostfix;
    property CloseButtonCaption: string read GetCloseButtonCaption write SetCloseButtonCaption;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    destructor Destroy; override;
    { Public declarations }
    class function CheckCompatiblity(const ConnStr, FormTittle: string; TerminateOnIncompatible: Boolean = True;
      TerminateTimeoutSec: integer = 120): TCheckVersionResult;
    class procedure PerformPeriodicallyChecking(const ConnStr, FormTittle: string;
      TerminateOnIncompatible: Boolean = True; TerminateTimeoutSec: integer = 120);
  end;

implementation

{$R *.dfm}

resourcestring
  rsVersionControl = '�������� ������: ';
  rsIncompatibledTittle =
    '���������� �� ����� ��������������,'#13#10'��������� ��� ������ �� ����� ����������� ������������� '#13#10'� ���������� ������� ���������������� �������. '#13#10'����������, �������� ����������';
  rsNotActualTittle =
    '� ���������� ������������ ������, �� ��������������� ���������� �������.'#13#10'����������, �������� ����������.';

  rsTerminateDelay = '����� �������� ����� ���� ���������� �����'#13#10'������������� ��������� ����� %d ������';
  rsTerminateImmediatelly = '����� �������� ����� ���� ���������� �����'#13#10'������������� ���������';

  rsCloseButtonIncompatibleCaption = '�������';
  rsCloseButtonPrevVerCaption = '����������';
  { TfmVersionManager }

class function TfmVersionManager.CheckCompatiblity(const ConnStr, FormTittle: string; TerminateOnIncompatible: Boolean;
  TerminateTimeoutSec: integer): TCheckVersionResult;
begin
  Result := TVersionControl.CheckInstanceCompatiblity(ConnStr, nil);
  if Result > cvrPrevWorked then
    Exit;

  FFormCaption := FormTittle;
  if not Assigned(FInstance) then
    FInstance := TfmVersionManager.Create;

  FInstance.DisplayNotActualVersions(ConnStr, cvrWTF);
  FInstance.SetUIText(Result, TerminateOnIncompatible, TerminateTimeoutSec);

  FInstance.ShowModal;

  if (Result <= cvrIncompatible) and (TerminateOnIncompatible) then
    FInstance.SetTerminateTimer(TerminateTimeoutSec);
end;

constructor TfmVersionManager.Create;
begin
  inherited Create(nil);
  Ffr := TfrVersionControl.Create(nil);
  Ffr.Parent := Self;
  Ffr.Align := alClient;
end;

procedure TfmVersionManager.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.WndParent := 0;
  Params.ExStyle := Params.ExStyle or WS_EX_APPWINDOW;
end;

class destructor TfmVersionManager.Destroy;
begin
  FInstance.Free;
  FInstance := nil;
end;

destructor TfmVersionManager.Destroy;
begin
  FInstance := nil;
  TVersionControl.UnregisterIncompatiblityDetectedCallback(IncompatibleCallback);
  inherited;
end;

procedure TfmVersionManager.DisplayNotActualVersions(const ConnStr: string; BelowStatus: TCheckVersionResult);
var
  Items: TVersionList;
  i: integer;
begin
  Items := TVersionList.Create;
  try
    TVersionControl.GetAllInstanceItemVersions(ConnStr, Items, True);

    for i := Items.Count - 1 downto 0 do
      if Items[i].IsCompatible >= BelowStatus then
        Items.Delete(i);

    Ffr.DisplayItems(Items);
  finally
    Items.Free;
  end;
end;

procedure TfmVersionManager.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

function TfmVersionManager.GetCloseButtonCaption: string;
begin
  Result := btnClose.Caption;
end;

function TfmVersionManager.GetPostfix: string;
begin
  Result := lblPostfix.Caption;
end;

function TfmVersionManager.GetTittle: string;
begin
  Result := lblTittle.Caption;
end;

procedure TfmVersionManager.IncompatibleCallback(Sender: TObject);
begin
  CheckCompatiblity(FConnStr, FFormCaption, FTerminateOnIncompatible, FTerminateTimeoutSec);
end;

class procedure TfmVersionManager.PerformPeriodicallyChecking(const ConnStr, FormTittle: string;
  TerminateOnIncompatible: Boolean = True; TerminateTimeoutSec: integer = 120);
begin
  if not Assigned(FInstance) then
    FInstance := TfmVersionManager.Create;
  FFormCaption := FormTittle;

  FInstance.RegisterPeriodicallyCheck(ConnStr, TerminateOnIncompatible, TerminateTimeoutSec);
end;

procedure TfmVersionManager.RegisterPeriodicallyCheck(const ConnStr: string; TerminateOnIncompatible: Boolean;
  TerminateTimeoutSec: integer);
begin
  FConnStr := ConnStr;
  FTerminateOnIncompatible := TerminateOnIncompatible;
  FTerminateTimeoutSec := TerminateTimeoutSec;
  TVersionControl.RegisterIncompatiblityDetectedCallback(ConnStr, IncompatibleCallback);
end;

procedure TfmVersionManager.SetCloseButtonCaption(const Value: string);
begin
  btnClose.Caption := Value;
end;

procedure TfmVersionManager.SetPostfix(const Value: string);
begin
  lblPostfix.Caption := Value;
end;

procedure TfmVersionManager.SetTerminateTimer(TerminateTimeoutSec: integer);
begin
  if TerminateTimeoutSec > 0 then
  begin
    tmrTerminate.Interval := TerminateTimeoutSec * 1000;
    tmrTerminate.Enabled := True;
  end
  else
    if Assigned(tmrTerminate.OnTimer) then
      tmrTerminate.OnTimer(Self);
end;

procedure TfmVersionManager.SetTittle(const Value: string);
begin
  lblTittle.Caption := Value;
end;

procedure TfmVersionManager.SetUIText(CheckResult: TCheckVersionResult; TerminateOnIncompatible: Boolean;
  TerminateTimeoutSec: integer);
begin
  Caption := rsVersionControl + FFormCaption;
  if CheckResult <= cvrIncompatible then
  begin
    Tittle := rsIncompatibledTittle;
    if TerminateOnIncompatible then
    begin
      if TerminateTimeoutSec > 0 then
        Postfix := Format(rsTerminateDelay, [TerminateTimeoutSec])
      else
        Postfix := rsTerminateImmediatelly;
      CloseButtonCaption := rsCloseButtonIncompatibleCaption;
    end;
  end
  else
  begin
    Tittle := rsNotActualTittle;
    Postfix := '';
    CloseButtonCaption := rsCloseButtonPrevVerCaption;
  end;
end;

procedure TfmVersionManager.tmrTerminateTimer(Sender: TObject);
begin
  TerminateProcess(GetCurrentProcess, 0);
end;

end.
