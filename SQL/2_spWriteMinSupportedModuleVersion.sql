-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		kami
-- Create date: 2017-12-28
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.spVersionControlWriteMinSupportedModuleVersion
	-- Add the parameters for the stored procedure here
	@ID int
	,@ModuleName nvarchar(255)
	,@MinSupportedVersion nvarchar(100)
	,@ActualVersion  nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @WritedID int
    -- Insert statements for procedure here
	IF EXISTS(SELECT ID FROM dbo.DBVersionControl WHERE ID = @ID)
	BEGIN
		UPDATE [dbo].[DBVersionControl]
			SET [ModuleName] = @ModuleName
				,[MinSupportedVersion] = @MinSupportedVersion
				,[ActualVersion] = @ActualVersion
			WHERE [ID] = @ID;
		SET @WritedID = @ID;
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[DBVersionControl]
			([ModuleName]
			,[MinSupportedVersion]
			,[ActualVersion])
		VALUES
			(@ModuleName
			,@MinSupportedVersion
			,@ActualVersion);
		SET @WritedID = SCOPE_IDENTITY();
	END;

	SELECT @WritedID AS ID;
END
GO

GRANT EXECUTE ON dbo.spVersionControlWriteMinSupportedModuleVersion TO [public]
GO