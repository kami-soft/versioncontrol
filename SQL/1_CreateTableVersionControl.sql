/****** Object:  Table [dbo].[DBVersionControl]    Script Date: 28.12.2017 23:30:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DBVersionControl](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [nvarchar](255) NOT NULL,
	[MinSupportedVersion] [nvarchar](100) NOT NULL,
	[ActualVersion]  [nvarchar](100) NOT NULL
 CONSTRAINT [PK_DBVersionControl] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Index [UNIQUE_ModuleName]    Script Date: 27.03.2018 10:15:48 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UNIQUE_ModuleName] ON [dbo].[DBVersionControl]
(
	[ModuleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


