/****** Object:  Table [dbo].[DBVersionControl]    Script Date: 28.12.2017 23:30:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'DBVersionControl'))
BEGIN
CREATE TABLE [dbo].[DBVersionControl](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [nvarchar](255) NOT NULL,
	[MinSupportedVersion] [nvarchar](100) NOT NULL,
	[ActualVersion]  [nvarchar](100) NOT NULL
 CONSTRAINT [PK_DBVersionControl] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Index [UNIQUE_ModuleName]    Script Date: 27.03.2018 10:15:48 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UNIQUE_ModuleName] ON [dbo].[DBVersionControl]
(
	[ModuleName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

END

GO

IF NOT EXISTS (SELECT
                     column_name
               FROM
                     INFORMATION_SCHEMA.columns
               WHERE
                     table_name = 'DBVersionControl'
                     AND column_name = 'Blacklist')
    ALTER TABLE [dbo].[DBVersionControl] ADD Blacklist varchar(max) NOT NULL DEFAULT ''
	
GO
	
	
/****** Object:  StoredProcedure [dbo].[spGetAllMinSupportedModules]    Script Date: 28.12.2017 23:58:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.spVersionControlGetAllMinSupportedModules') IS NULL
EXEC('CREATE PROCEDURE dbo.spVersionControlGetAllMinSupportedModules AS SET NOCOUNT ON;')
GO

-- =============================================
-- Author:		kami
-- Create date: 2017-12-28
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spVersionControlGetAllMinSupportedModules] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ID], [ModuleName], [MinSupportedVersion], [ActualVersion], [Blacklist]
	FROM [dbo].[DBVersionControl]

END
GO

GRANT EXECUTE ON dbo.[spVersionControlGetAllMinSupportedModules] TO [public]
GO

/****** Object:  StoredProcedure [dbo].[spGetMinSupportedModuleVersion]    Script Date: 28.12.2017 23:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('dbo.spVersionControlGetMinSupportedModuleVersion') IS NULL
EXEC('CREATE PROCEDURE dbo.spVersionControlGetMinSupportedModuleVersion AS SET NOCOUNT ON;')
GO

-- =============================================
-- Author:		kami
-- Create date: 2017-12-28
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spVersionControlGetMinSupportedModuleVersion]
	-- Add the parameters for the stored procedure here
	@ModuleName nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	  [ID]
	  ,[ModuleName]
	  ,[MinSupportedVersion]
	  ,[ActualVersion]
	  ,[Blacklist]
	FROM
	  dbo.DBVersionControl
	WHERE
	  ModuleName = @ModuleName
END
GO

GRANT EXECUTE ON dbo.[spVersionControlGetMinSupportedModuleVersion] TO [public]
GO


-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.spVersionControlWriteMinSupportedModuleVersion') IS NULL
EXEC('CREATE PROCEDURE dbo.spVersionControlWriteMinSupportedModuleVersion AS SET NOCOUNT ON;')
GO

-- =============================================
-- Author:		kami
-- Create date: 2017-12-28
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE dbo.spVersionControlWriteMinSupportedModuleVersion
	-- Add the parameters for the stored procedure here
	@ID int
	,@ModuleName nvarchar(255)
	,@MinSupportedVersion nvarchar(100)
	,@ActualVersion  nvarchar(100)
	,@Blacklist varchar(max) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @WritedID int
    -- Insert statements for procedure here
	IF EXISTS(SELECT ID FROM dbo.DBVersionControl WHERE ID = @ID)
	BEGIN
		UPDATE [dbo].[DBVersionControl]
			SET [ModuleName] = @ModuleName
				,[MinSupportedVersion] = @MinSupportedVersion
				,[ActualVersion] = @ActualVersion
				,[Blacklist] = @Blacklist
			WHERE [ID] = @ID;
		SET @WritedID = @ID;
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[DBVersionControl]
			([ModuleName]
			,[MinSupportedVersion]
			,[ActualVersion]
			,[Blacklist])
		VALUES
			(@ModuleName
			,@MinSupportedVersion
			,@ActualVersion
			,@Blacklist);
		SET @WritedID = SCOPE_IDENTITY();
	END;

	SELECT @WritedID AS ID;
END
GO

GRANT EXECUTE ON dbo.spVersionControlWriteMinSupportedModuleVersion TO [public]
GO




-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.spVersionControlRegisterModuleVersion') IS NULL
EXEC('CREATE PROCEDURE dbo.spVersionControlRegisterModuleVersion AS SET NOCOUNT ON;')
GO


-- =============================================
-- Author:		kami
-- Create date: 2018-03-17
-- Description:	регистрация версии модуля. Если модуля нет в БД - запись будет создана.
-- =============================================
ALTER PROCEDURE [dbo].[spVersionControlRegisterModuleVersion]
	-- Add the parameters for the stored procedure here
	@ModuleName_ nvarchar(255)
	,@MinSupportedVersion_ nvarchar(100)
	,@ActualVersion_  nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @ID_Module_ int
	DECLARE @BlackList_ varchar(max)

	SELECT @ID_Module_ = ID, @BlackList_ = [BlackList] FROM dbo.DBVersionControl WHERE [ModuleName] = @ModuleName_;

	SET  @ID_Module_ = ISNULL(@ID_Module_, 0);
	SET @BlackList_ = ISNULL(@BlackList_, '');

	EXEC [dbo].[spVersionControlWriteMinSupportedModuleVersion] @ID = @ID_Module_, @ModuleName  = @ModuleName_, @MinSupportedVersion = @MinSupportedVersion_, @ActualVersion = @ActualVersion_, @BlackList = @BlackList_
END
GO

GRANT EXECUTE  ON [dbo].[spVersionControlRegisterModuleVersion]  TO [public]
GO


-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.spVersionControlDeleteModule') IS NULL
EXEC('CREATE PROCEDURE dbo.spVersionControlDeleteModule AS SET NOCOUNT ON;')
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE dbo.spVersionControlDeleteModule
	-- Add the parameters for the stored procedure here
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.DBVersionControl WHERE ID = @ID
END
GO

GRANT EXECUTE ON dbo.spVersionControlDeleteModule TO [public]
GO