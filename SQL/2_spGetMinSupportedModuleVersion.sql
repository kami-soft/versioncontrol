/****** Object:  StoredProcedure [dbo].[spGetMinSupportedModuleVersion]    Script Date: 28.12.2017 23:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		kami
-- Create date: 2017-12-28
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spVersionControlGetMinSupportedModuleVersion]
	-- Add the parameters for the stored procedure here
	@ModuleName nvarchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	  [ID]
	  ,[ModuleName]
	  ,[MinSupportedVersion]
	  ,[ActualVersion]
	FROM
	  dbo.DBVersionControl
	WHERE
	  ModuleName = @ModuleName
END


GRANT EXECUTE ON dbo.[spVersionControlGetMinSupportedModuleVersion] TO [public]
GO