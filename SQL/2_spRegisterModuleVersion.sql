-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		kami
-- Create date: 2018-03-17
-- Description:	����������� ������ ������. ���� ������ ��� � �� - ������ ����� �������.
-- =============================================
CREATE PROCEDURE [dbo].[spVersionControlRegisterModuleVersion]
	-- Add the parameters for the stored procedure here
	@ModuleName_ nvarchar(255)
	,@MinSupportedVersion_ nvarchar(100)
	,@ActualVersion_  nvarchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @ID_Module_ int

	SELECT @ID_Module_ = ID FROM dbo.DBVersionControl WHERE [ModuleName] = @ModuleName_;

	SET  @ID_Module_ = ISNULL(@ID_Module_, 0);

	EXEC [dbo].[spVersionControlWriteMinSupportedModuleVersion] @ID = @ID_Module_, @ModuleName  = @ModuleName_, @MinSupportedVersion = @MinSupportedVersion_, @ActualVersion = @ActualVersion_
END
GO

GRANT EXECUTE  ON [dbo].[spVersionControlRegisterModuleVersion]  TO [public]
GO