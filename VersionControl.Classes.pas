unit VersionControl.Classes;

/// author: kami, 2017
///
/// version control unit.
/// help to understand that main program (exe) and additional modules (dll)
/// are compatible.
/// The compatiblity based on resourcestrings like
///
/// resourcestring
/// SomeModuleMinimalSupportedVersion = 'VersionControlEx:twain_32.dll=1.3.5.6';
///
/// where:
/// "VersionControlEx:" - key phrase (do not change!, use as-is)
/// "twain_32.dll" - name of module to check compatiblity
/// "1.3.5.6" - minimal supported module version
/// do not forget that you should use this resourcestring at least once in
/// your code (otherwise compiler exclude this string from resources).
///
/// in addition - you can check the database compatiblity of each module
/// (create DB components by using SQL scripts).
interface

uses
  System.Classes,
  System.Generics.Collections,
  Data.DB,
  Data.Win.ADODB,
  Vcl.ExtCtrls,
  pbPublic,
  pbInput,
  pbOutput,
  uAbstractProtoBufClasses;

type
  TCheckVersionResult = (cvrBlocked, // current version is in blacklist
    cvrIncompatible, // current version is less than minimal
    cvrPrevWorked, // current version is less than actual, but greater than minimal
    cvrWTF, // current version greater than actual
    cvrActual, // current version equal to actual
    cvrNoControl); // do not control this module.

  TVersion = packed record
    constructor Create(const AStr: string);
    function ToString: string;
    procedure FromString(const AStr: string);

    function CompareTo(const AVer: TVersion): integer;
    case integer of
      0:
        (Build, Release, Minor, Major: Word);
      1:
        (DetailedItems: array [0 .. 3] of Word);
      2:
        (HiVer, LoVer: Cardinal);
      3:
        (Items: array [0 .. 1] of Cardinal);
      4:
        (FullVer: UInt64);
  end;

  /// version compatiblity is determined as:
  /// if MinSupportedVersion (declared in resources or DB) > 0 then
  /// ModuleVersion must be >= MinSupportedVersion
  /// if MinSupportedVersion = 0 then
  /// ModuleVersion must be > 0 (to enshure that module really exists)
  TVersionControlItem = class(TAbstractProtoBuf)
  strict private
    FMainProgramItem: Boolean;
    FDBID: integer;
    FModuleName: string;
    FModuleCurrentVersion: TVersion;
    FModuleActualVersion: TVersion;
    FMinInstSupportedVersion: TVersion;
    FMinDBSupportedVersion: TVersion;

    FVersionBlacklist: TStrings;

    procedure ReadFromDB(Conn: TADOConnection);
  private
    procedure DeleteFromDB(Conn: TADOConnection);
    procedure WriteToDB(Conn: TADOConnection);
    procedure FillModuleVersions(Conn: TADOConnection);
  protected
    procedure ReadFromDS(DS: TDataSet); virtual;
    procedure ReadFileModuleVersion; virtual;
  strict protected
    procedure BeforeLoad; override;

    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    class procedure UseSelf;

    constructor Create(const AModuleName: string; const AMinInstSupportedVersion: TVersion); reintroduce;
    destructor Destroy; override;

    function IsCompatible: TCheckVersionResult;

    property MainProgramItem: Boolean read FMainProgramItem write FMainProgramItem;
    property ModuleName: string read FModuleName write FModuleName;
    property ModuleCurrentVersion: TVersion read FModuleCurrentVersion;
    property ModuleActualVersion: TVersion read FModuleActualVersion;
    property MinInstSupportedVersion: TVersion read FMinInstSupportedVersion;
    property MinDBSupportedVersion: TVersion read FMinDBSupportedVersion;

    property VersionBlacklist: TStrings read FVersionBlacklist;
  end;

  TVersionControlItemClass = class of TVersionControlItem;

  TVersionList = class(TProtoBufList<TVersionControlItem>)
  private
    procedure RemoveNotUsedInDB;

    function AddItem(const AModuleName: string; const AMinSupportedVersion: TVersion): TVersionControlItem;
    procedure FillModuleVersions(Conn: TADOConnection);
    procedure LoadFromDB(Conn: TADOConnection);

    procedure UnionDependedModuleVersions;

    function FindItem(const AModuleName: string): TVersionControlItem;

    procedure LoadFromBuf(ProtoBuf: TProtoBufInput);
  public
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);

    function PoorestCompatiblity: TCheckVersionResult;
  end;

  TVersionControl = class(TObject)
  strict private
  const
    cCheckInterval = 1000 * 60 * 1;
  strict private
    FConnStr: string;
    FTimer: TTimer;
    FCallbackList: TList<TNotifyEvent>;

    procedure OnTimerCallbackCheck(Sender: TObject);

    constructor Create(const AConnStr: string);
  strict private
    class var FInstance: TVersionControl;
  private
    class var FItemClass: TVersionControlItemClass;
    class function ItemClass: TVersionControlItemClass;
  public
    class destructor Destroy;
    destructor Destroy; override;
    /// <summary>
    /// Check all published modules for compatiblity.
    /// </summary>
    /// <param name="ConnStr">ADO connection string for DB. Can be empty. If defined then DB must contains needed stored proc`s and tables</param>
    /// <param name="NotCompatibledItems">Returns list of not compatibled (by instance or DB) items. Can be nil</param>
    /// <returns>True - if all modules are compatible (NotCompatibledItems returns empty). False - if not (NotCompatibledItems contains list of incompatible items)</returns>
    class function CheckInstanceCompatiblity(const ConnStr: string; NotCompatibledItems: TVersionList = nil): TCheckVersionResult;
    /// <summary>
    /// return all published modules version info
    /// </summary>
    /// <param name="ConnStr">ADO connection string for DB. Can be empty. If defined then DB must contains needed stored proc`s and tables</param>
    /// <param name="AllItems">Returns list of modules version info. Each item contains current, minimal supported and DB supported versions</param>
    class procedure GetAllInstanceItemVersions(const ConnStr: string; AllItems: TVersionList; RemoveNotUsedInDB: Boolean = True; const MainFileName: string = '');

    class procedure GetAllDBVersions(const ConnStr: string; AllItems: TVersionList);
    class procedure WriteVersionItemToDB(const ConnStr: string; Item: TVersionControlItem);
    class procedure DeleteItemFromDB(const ConnStr: string; Item: TVersionControlItem);

    class procedure RegisterAllCurrentVersionsAsActual(const ConnStr: string);

    /// <summary>
    /// register callback proc for periodically (by default - 10 minutes, see cCheckInterval const) incompatiblity check
    /// if some modules become incompatible state - calls all callback proc
    /// and continue periodically checking
    /// </summary>
    /// <param name="AConnStr">used only once to set database ADO connection string. See comments for GetAllInstanceItemVersions</param>
    /// <param name="CallbackProc">callback procedure. When no longer needed (e.g. object destroyed)- you MUST call Unregister proc</param>
    class procedure RegisterIncompatiblityDetectedCallback(const AConnStr: string; CallbackProc: TNotifyEvent);
    /// <summary>
    /// unregister callback proc for periodically incompatiblity check
    /// Must be called when object (which contains callback proc) destroyed
    /// or callback no longer needed
    /// </summary>
    class procedure UnregisterIncompatiblityDetectedCallback(CallbackProc: TNotifyEvent);
  end;

implementation

uses
  System.SysUtils,
  System.StrUtils,
  Winapi.Windows,
  uThreadSafeLogger;

const
  ˝VersionControlPhrase = 'VersionControlEx';

function ReadResStr(Inst: hModule; ID: NativeUInt): string;
var
  Buf: array [0 .. 255] of Char;
begin
  if LoadString(Inst, ID, @Buf, Length(Buf)) <> 0 then
    Result := Buf
  else
    Result := '';
end;

procedure ParseStringValue(const strValue: string; Items: TVersionList);
var
  s: string;
  i: integer;

  sModuleName: string;
  sMinInstModuleVersion: string;
begin
  // strValue = 'VersionControlEx:access.dll=2.3.5.6';
  s := Trim(strValue);
  if not AnsiStartsStr(˝VersionControlPhrase, s) then
    exit;
  s := Trim(StringReplace(s, ˝VersionControlPhrase, '', [rfIgnoreCase]));
  if not AnsiStartsStr(':', s) then
    exit;
  // strValue = ':access.dll=2.3.5.6';

  s := Trim(StringReplace(s, ':', '', []));
  // strValue = 'access.dll=2.3.5.6';
  i := Pos('=', s);
  if i = 0 then
    exit;

  sModuleName := Trim(Copy(s, 1, i - 1));
  sMinInstModuleVersion := Trim(Copy(s, i + 1, Length(s)));

  Items.AddItem(sModuleName, TVersion.Create(sMinInstModuleVersion));
end;

procedure ParseResString(Inst: hModule; ResID: NativeUInt; Items: TVersionList);
var
  Stream: TResourceStream;

  Min: integer;
  Max: integer;
  Index: integer;
  Buffer: PChar;
  Len: Word;
begin
  Min := (ResID - 1) shl 4;
  Max := Min or $0F;

  Stream := TResourceStream.CreateFromID(Inst, ResID, RT_STRING);
  try
    Buffer := Stream.Memory;
    for index := Min to Max do
      begin
        // determine the length of the string
        Len := Word(Buffer^);
        if Len > 0 then
          begin
            ParseStringValue(ReadResStr(Inst, index), Items);
            Inc(Buffer, Len + 1);
          end
        else
          Inc(Buffer);
      end;
  finally
    Stream.Free;
  end;
end;

function EnumResNameProc(hModule: hModule; lpszType, lpszName: PChar; lParam: LONG_PTR): BOOL; stdcall;
var
  Items: TVersionList;
begin
  Result := True;
  Items := TVersionList(lParam);
  if Is_IntResource(lpszName) then
    ParseResString(hModule, NativeUInt(lpszName), Items);
end;

{ TVersion }

function TVersion.CompareTo(const AVer: TVersion): integer;
begin
  if Self.FullVer > AVer.FullVer then
    Result := 1
  else
    if Self.FullVer < AVer.FullVer then
      Result := -1
    else
      Result := 0;
end;

constructor TVersion.Create(const AStr: string);
begin
  inherited;
  FromString(AStr);
end;

procedure TVersion.FromString(const AStr: string);
var
  SIP: string;
  Start: integer;
  i: integer;
  Index: integer;
  Count: integer;
  SGroup: string;
  G: integer;
begin
  FullVer := 0;
  SIP := AStr + '.';
  Start := 1;
  for i := 3 downto 0 do
    begin
      index := PosEx('.', SIP, Start);
      if index = 0 then
        Break;
      Count := index - Start + 1;
      SGroup := Copy(SIP, Start, Count - 1);
      if TryStrToInt(SGroup, G) and (G >= 0) and (G <= MAXWORD) then
        DetailedItems[i] := G;
      Inc(Start, Count);
    end;
end;

function TVersion.ToString: string;
begin
  Result := Format('%d.%d.%d.%d', [Major, Minor, Release, Build]);
end;

{ TVersionControlItem }

procedure TVersionControlItem.BeforeLoad;
begin
  inherited;
  FDBID := 0;
  FModuleName := '';
  FModuleCurrentVersion.FullVer := 0;
  FModuleActualVersion.FullVer := 0;
  FMinInstSupportedVersion.FullVer := 0;
  FMinDBSupportedVersion.FullVer := 0;
end;

constructor TVersionControlItem.Create(const AModuleName: string; const AMinInstSupportedVersion: TVersion);
begin
  inherited Create;
  FModuleName := AModuleName;
  FMinInstSupportedVersion := AMinInstSupportedVersion;

  FVersionBlacklist := TStringList.Create;
end;

procedure TVersionControlItem.DeleteFromDB(Conn: TADOConnection);
var
  sp: TADOStoredProc;
begin
  sp := TADOStoredProc.Create(nil);
  try
    sp.Connection := Conn;
    sp.ProcedureName := 'spVersionControlDeleteModule';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@ID').Value := FDBID;
    sp.ExecProc;
    FDBID := 0;
    FMinDBSupportedVersion.FullVer := 0;
    FModuleActualVersion.FullVer := 0;
  finally
    sp.Free;
  end;
end;

destructor TVersionControlItem.Destroy;
begin
  FreeAndNil(FVersionBlacklist);
  inherited;
end;

procedure TVersionControlItem.FillModuleVersions(Conn: TADOConnection);
begin
  ReadFileModuleVersion;
  ReadFromDB(Conn);
end;

function TVersionControlItem.IsCompatible: TCheckVersionResult;
  function InternalIsCompatible(const MinSupportedVersion: TVersion): Boolean;
  begin
    if MinSupportedVersion.FullVer > 0 then
      Result := ModuleCurrentVersion.CompareTo(MinSupportedVersion) >= 0
    else
      Result := ModuleCurrentVersion.FullVer > 0;
  end;

  function IsInBlacklist: Boolean;
  var
    i: integer;
    VerMin, VerMax: TVersion;
  begin
    Result := False;
    for i := 0 to FVersionBlacklist.Count - 1 do
      begin
        VerMin.FromString(FVersionBlacklist.Names[i]);
        VerMax.FromString(FVersionBlacklist.ValueFromIndex[i]);

        Result := (FModuleCurrentVersion.CompareTo(VerMin) >= 0) and (FModuleCurrentVersion.CompareTo(VerMax) <= 0);
        if Result then
          Break;
      end;
  end;

begin
  if (MinDBSupportedVersion.FullVer = 0) and FMainProgramItem then
    Result := cvrIncompatible
  else
    if MinDBSupportedVersion.FullVer = 0 then
      Result := cvrNoControl
    else
      if not InternalIsCompatible(MinInstSupportedVersion) or not InternalIsCompatible(MinDBSupportedVersion) then
        Result := cvrIncompatible
      else
        if ModuleCurrentVersion.CompareTo(ModuleActualVersion) < 0 then
          Result := cvrPrevWorked
        else
          if ModuleCurrentVersion.CompareTo(ModuleActualVersion) > 0 then
            Result := cvrWTF
          else
            Result := cvrActual;

  if IsInBlacklist then
    Result := cvrBlocked;
end;

function TVersionControlItem.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    1:
      FDBID := ProtoBuf.readInt32;
    2:
      FModuleName := ProtoBuf.readString;
    3:
      FModuleCurrentVersion.FullVer := ProtoBuf.readInt64;
    4:
      FMinInstSupportedVersion.FullVer := ProtoBuf.readInt64;
    5:
      FMinDBSupportedVersion.FullVer := ProtoBuf.readInt64;
    6:
      FModuleActualVersion.FullVer := ProtoBuf.readInt64;
  end;
  Result := FieldNumber in [1 .. 6];
end;

procedure TVersionControlItem.ReadFileModuleVersion;
var
  Info: Pointer;
  InfoSize: DWORD;
  FileInfo: PVSFixedFileInfo;
  FileInfoSize: DWORD;
  Tmp: DWORD;
begin
  InfoSize := GetFileVersionInfoSize(PChar(FModuleName), Tmp);

  if InfoSize > 0 then
    begin
      GetMem(Info, InfoSize);
      try
        if GetFileVersionInfo(PChar(FModuleName), 0, InfoSize, Info) then
          if VerQueryValue(Info, '\', Pointer(FileInfo), FileInfoSize) then
            begin
              FModuleCurrentVersion.Items[1] := FileInfo.dwFileVersionMS;
              FModuleCurrentVersion.Items[0] := FileInfo.dwFileVersionLS;
            end;
      finally
        FreeMem(Info, FileInfoSize);
      end;
    end;
end;

procedure TVersionControlItem.ReadFromDB(Conn: TADOConnection);
var
  sp: TADOStoredProc;
begin
  if not Assigned(Conn) then
    exit;

  sp := TADOStoredProc.Create(nil);
  try
    sp.Connection := Conn;
    sp.ProcedureName := 'spVersionControlGetMinSupportedModuleVersion';
    sp.Parameters.CreateParameter('@ModuleName', ftString, pdInput, 100, ModuleName);
    sp.Open;
    try
      ReadFromDS(sp);
    finally
      sp.Close;
    end;
  finally
    sp.Free;
  end;
end;

procedure TVersionControlItem.ReadFromDS(DS: TDataSet);
begin
  if not DS.Eof then
    begin
      FDBID := DS.FieldByName('ID').AsInteger;
      FModuleName := DS.FieldByName('ModuleName').AsString;
      FMinDBSupportedVersion.FromString(DS.FieldByName('MinSupportedVersion').AsString);
      FModuleActualVersion.FromString(DS.FieldByName('ActualVersion').AsString);
      if Assigned(DS.FindField('Blacklist')) then
        FVersionBlacklist.Text := DS.FieldByName('Blacklist').AsString;
    end
  else
    begin
      FDBID := 0;
      FModuleActualVersion.FullVer := 0;
      FMinDBSupportedVersion.FullVer := 0;
    end;
end;

procedure TVersionControlItem.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt32(1, FDBID);
  ProtoBuf.writeString(2, FModuleName);
  ProtoBuf.writeInt64(3, FModuleCurrentVersion.FullVer);
  ProtoBuf.writeInt64(4, FMinInstSupportedVersion.FullVer);
  ProtoBuf.writeInt64(5, FMinDBSupportedVersion.FullVer);
  ProtoBuf.writeInt64(6, FModuleActualVersion.FullVer);
end;

class procedure TVersionControlItem.UseSelf;
begin
  TVersionControl.FItemClass := Self;
end;

procedure TVersionControlItem.WriteToDB(Conn: TADOConnection);
var
  sp: TADOStoredProc;
begin
  sp := TADOStoredProc.Create(nil);
  try
    sp.Connection := Conn;
    sp.ProcedureName := 'spVersionControlWriteMinSupportedModuleVersion';
    sp.Parameters.Refresh;
    sp.Parameters.ParamByName('@ID').Value := FDBID;
    sp.Parameters.ParamByName('@ModuleName').Value := ModuleName;
    sp.Parameters.ParamByName('@MinSupportedVersion').Value := FMinDBSupportedVersion.ToString;
    sp.Parameters.ParamByName('@ActualVersion').Value := FModuleActualVersion.ToString;
    sp.Parameters.ParamByName('@Blacklist').Value := FVersionBlacklist.Text;
    sp.Open;
    try
      FDBID := sp.FieldByName('ID').AsInteger;
    finally
      sp.Close;
    end;
  finally
    sp.Free;
  end;
end;

{ TVersionList }

function TVersionList.AddItem(const AModuleName: string; const AMinSupportedVersion: TVersion): TVersionControlItem;
begin
  Result := FindItem(AModuleName);
  if not Assigned(Result) then
    begin
      Result := TVersionControl.ItemClass.Create(AModuleName, AMinSupportedVersion);
      Add(Result);
    end
  else
    begin
      if Result.MinInstSupportedVersion.CompareTo(AMinSupportedVersion) < 0 then
        Result.MinInstSupportedVersion.FromString(AMinSupportedVersion.ToString);
    end;
end;

procedure TVersionList.FillModuleVersions(Conn: TADOConnection);
var
  i: integer;
begin
  for i := 0 to Count - 1 do
    Items[i].FillModuleVersions(Conn);
end;

function TVersionList.FindItem(const AModuleName: string): TVersionControlItem;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if AnsiSameText(AModuleName, Items[i].ModuleName) then
      begin
        Result := Items[i];
        Break;
      end;
end;

procedure TVersionList.LoadFromBuf(ProtoBuf: TProtoBufInput);
var
  FieldNumber: integer;
  Tag: integer;
begin
  Clear;
  Tag := ProtoBuf.readTag;
  while Tag <> 0 do
    begin
      FieldNumber := getTagFieldNumber(Tag);
      if FieldNumber = 1 then
        begin
          AddFromBuf(ProtoBuf, FieldNumber);
        end
      else
        ProtoBuf.skipField(Tag);
      Tag := ProtoBuf.readTag;
    end;
end;

procedure TVersionList.LoadFromDB(Conn: TADOConnection);
var
  sp: TADOStoredProc;
  Item: TVersionControlItem;
begin
  Clear;
  sp := TADOStoredProc.Create(nil);
  try
    sp.Connection := Conn;
    sp.ProcedureName := 'spVersionControlGetAllMinSupportedModules';
    sp.Open;
    try
      while not sp.Eof do
        begin
          if not Assigned(FindItem(sp.FieldByName('ModuleName').AsString)) then
            begin
              Item := AddItem(sp.FieldByName('ModuleName').AsString, TVersion.Create(''));
              Add(Item);
              Item.ReadFromDS(sp);
            end;
          sp.Next;
        end;
    finally
      sp.Close;
    end;
  finally
    sp.Free;
  end;
end;

procedure TVersionList.LoadFromStream(Stream: TStream);
var
  pb: TProtoBufInput;
  tmpStream: TStream;
begin
  pb := TProtoBufInput.Create;
  try
    tmpStream := TMemoryStream.Create;
    try
      tmpStream.CopyFrom(Stream, Stream.Size - Stream.Position);
      tmpStream.Seek(0, soBeginning);
      pb.LoadFromStream(tmpStream);
    finally
      tmpStream.Free;
    end;
    LoadFromBuf(pb);
  finally
    pb.Free;
  end;
end;

function TVersionList.PoorestCompatiblity: TCheckVersionResult;
var
  i: integer;
  Tmp: TCheckVersionResult;
begin
  Result := high(TCheckVersionResult);
  for i := 0 to Count - 1 do
    begin
      Tmp := Items[i].IsCompatible;
      if Tmp < Result then
        Result := Tmp;
    end;
end;

procedure TVersionList.RemoveNotUsedInDB;
var
  i: integer;
begin
  for i := Count - 1 downto 0 do
    if (Items[i].MinDBSupportedVersion.FullVer = 0) and (not(Items[i].MainProgramItem)) then
      Delete(i);
end;

procedure TVersionList.SaveToStream(Stream: TStream);
var
  pb: TProtoBufOutput;
begin
  pb := TProtoBufOutput.Create;
  try
    SaveToBuf(pb, 1);
    pb.SaveToStream(Stream);
  finally
    pb.Free;
  end;
end;

procedure TVersionList.UnionDependedModuleVersions;
  procedure GetItemNestVersions(Item: TVersionControlItem; nestVersions: TVersionList);
  const
    LOAD_LIBRARY_AS_IMAGE_RESOURCE = $20;
  var
    h: THandle;
  begin
    h := LoadLibraryEx(PChar(Item.ModuleName), 0, LOAD_LIBRARY_AS_DATAFILE or LOAD_LIBRARY_AS_IMAGE_RESOURCE);
    if h = 0 then
      exit;
    try
      EnumResourceNames(h, RT_STRING, @EnumResNameProc, NativeInt(nestVersions));
    finally
      if not FreeLibrary(h) then
        RaiseLastOSError;
    end;
  end;

  procedure UpdateSelf(nestVersions: TVersionList);
  var
    NestItem: TVersionControlItem;
    SelfItem: TVersionControlItem;
    i: integer;
  begin
    for i := nestVersions.Count - 1 downto 0 do
      begin
        NestItem := nestVersions[i];
        SelfItem := FindItem(NestItem.ModuleName);
        if not Assigned(SelfItem) then
          begin
            Add(nestVersions.Extract(NestItem));
          end
        else
          begin
            if SelfItem.ModuleCurrentVersion.CompareTo(NestItem.ModuleCurrentVersion) < 0 then
              SelfItem.ModuleCurrentVersion.FromString(NestItem.ModuleCurrentVersion.ToString);
            if SelfItem.MinInstSupportedVersion.CompareTo(NestItem.MinInstSupportedVersion) < 0 then
              SelfItem.MinInstSupportedVersion.FromString(NestItem.MinInstSupportedVersion.ToString);
            if SelfItem.MinDBSupportedVersion.CompareTo(NestItem.MinDBSupportedVersion) < 0 then
              SelfItem.MinDBSupportedVersion.FromString(NestItem.MinDBSupportedVersion.ToString);
          end;
      end;
  end;

var
  nestVersion: TVersionList;
  i: integer;
begin
  nestVersion := TVersionList.Create;
  try
    i := 0;
    while i < Count do
      begin
        nestVersion.Clear;
        GetItemNestVersions(Items[i], nestVersion);
        UpdateSelf(nestVersion);
        Inc(i);
      end;
  finally
    nestVersion.Free;
  end;
end;

{ TVersionControl }

class function TVersionControl.CheckInstanceCompatiblity(const ConnStr: string; NotCompatibledItems: TVersionList): TCheckVersionResult;
var
  i: integer;
  Item: TVersionControlItem;
  AllItems: TVersionList;
  cvr: TCheckVersionResult;
begin
  Result := cvrActual;
  if Assigned(NotCompatibledItems) then
    NotCompatibledItems.Clear;

  AllItems := TVersionList.Create;
  try
    GetAllInstanceItemVersions(ConnStr, AllItems);
    for i := AllItems.Count - 1 downto 0 do
      begin
        Item := AllItems[i];
        cvr := Item.IsCompatible;
        if cvr < Result then
          Result := cvr;
        if Assigned(NotCompatibledItems) and (cvr <= cvrPrevWorked) then
          NotCompatibledItems.Add(AllItems.Extract(Item));
      end;
  finally
    AllItems.Free;
  end;
end;

constructor TVersionControl.Create(const AConnStr: string);
begin
  inherited Create;
  FConnStr := AConnStr;
  FTimer := TTimer.Create(nil);
  FTimer.OnTimer := OnTimerCallbackCheck;
  FTimer.Interval := cCheckInterval;
  FTimer.Enabled := True;

  FCallbackList := TList<TNotifyEvent>.Create;
end;

class destructor TVersionControl.Destroy;
begin
  FInstance.Free;
  FInstance := nil;
end;

class procedure TVersionControl.DeleteItemFromDB(const ConnStr: string; Item: TVersionControlItem);
var
  Conn: TADOConnection;
begin
  Conn := TADOConnection.Create(nil);
  try
    Conn.ConnectionString := ConnStr;
    Conn.LoginPrompt := False;
    Item.DeleteFromDB(Conn);
  finally
    Conn.Free;
  end;
end;

destructor TVersionControl.Destroy;
begin
  FTimer.Free;
  FTimer := nil;

  FCallbackList.Free;
  FCallbackList := nil;
  inherited;
end;

class procedure TVersionControl.GetAllDBVersions(const ConnStr: string; AllItems: TVersionList);
var
  Conn: TADOConnection;
begin
  Conn := TADOConnection.Create(nil);
  try
    Conn.ConnectionString := ConnStr;
    Conn.LoginPrompt := False;
    AllItems.LoadFromDB(Conn);
  finally
    Conn.Free;
  end;
end;

class procedure TVersionControl.GetAllInstanceItemVersions(const ConnStr: string; AllItems: TVersionList; RemoveNotUsedInDB: Boolean; const MainFileName: string);
var
  Conn: TADOConnection;
  s: string;
begin
  AllItems.Clear;

  if MainFileName = '' then
    s := ParamStr(0)
  else
    s := MainFileName;
  AllItems.AddItem(ExtractFileName(s), TVersion.Create('')).MainProgramItem := True;

  if ConnStr <> '' then
    begin
      Conn := TADOConnection.Create(nil);
      try
        Conn.ConnectionString := ConnStr;
        Conn.LoginPrompt := False;

        AllItems.UnionDependedModuleVersions;
        AllItems.FillModuleVersions(Conn);

        if RemoveNotUsedInDB then
          AllItems.RemoveNotUsedInDB;
      finally
        Conn.Free;
      end;
    end
  else
    begin
      AllItems.UnionDependedModuleVersions;
      AllItems.FillModuleVersions(nil);
    end;
end;

class function TVersionControl.ItemClass: TVersionControlItemClass;
begin
  if Assigned(FItemClass) then
    Result := FItemClass
  else
    Result := TVersionControlItem;
end;

procedure TVersionControl.OnTimerCallbackCheck(Sender: TObject);
var
  i: integer;
begin
  try
    if TVersionControl.CheckInstanceCompatiblity(FConnStr, nil) <= cvrIncompatible then
      begin
        FTimer.Enabled := False;
        try
          for i := FCallbackList.Count - 1 downto 0 do
            try
              FCallbackList[i](Self);
            except
              on e: Exception do
                LogFileX.LogException(e);
            end;
        finally
          FTimer.Enabled := True;
        end;
      end;
  except

  end;
end;

class procedure TVersionControl.RegisterAllCurrentVersionsAsActual(const ConnStr: string);
var
  Items: TVersionList;
  Item: TVersionControlItem;
  i: integer;
begin
  Items := TVersionList.Create;
  try
    GetAllInstanceItemVersions(ConnStr, Items, False);

    for i := 0 to Items.Count - 1 do
      begin
        Item := Items[i];
        if (Item.ModuleCurrentVersion.CompareTo(Item.ModuleActualVersion) > 0) then
          begin
            Item.ModuleActualVersion.FromString(Item.ModuleCurrentVersion.ToString);
            WriteVersionItemToDB(ConnStr, Items[i]);
          end;
      end;
  finally
    Items.Free;
  end;
end;

class procedure TVersionControl.RegisterIncompatiblityDetectedCallback(const AConnStr: string; CallbackProc: TNotifyEvent);
begin
  if not Assigned(FInstance) then
    FInstance := TVersionControl.Create(AConnStr);
  if not FInstance.FCallbackList.Contains(CallbackProc) then
    FInstance.FCallbackList.Add(CallbackProc);
end;

class procedure TVersionControl.UnregisterIncompatiblityDetectedCallback(CallbackProc: TNotifyEvent);
begin
  if Assigned(FInstance) then
    FInstance.FCallbackList.Remove(CallbackProc);
end;

class procedure TVersionControl.WriteVersionItemToDB(const ConnStr: string; Item: TVersionControlItem);
var
  Conn: TADOConnection;
begin
  Conn := TADOConnection.Create(nil);
  try
    Conn.ConnectionString := ConnStr;
    Conn.LoginPrompt := False;
    Item.WriteToDB(Conn);
  finally
    Conn.Free;
  end;
end;

end.
