unit VersionControl.MainDBEditorForm;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  VirtualTrees,
  Vcl.StdCtrls,
  Vcl.ExtCtrls,
  VersionControl.Classes;

type
  TfmMain = class(TForm)
    vtVersions: TVirtualStringTree;
    pnlTop: TPanel;
    btnAddNew: TButton;
    btnEditSelected: TButton;
    btnDBConnection: TButton;
    btnDeleteSelected: TButton;
    procedure btnDBConnectionClick(Sender: TObject);
    procedure vtVersionsFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure vtVersionsGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType; var CellText: string);
    procedure vtVersionsNodeDblClick(Sender: TBaseVirtualTree; const HitInfo: THitInfo);
    procedure btnAddNewClick(Sender: TObject);
    procedure btnEditSelectedClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure pnlTopClick(Sender: TObject);
    procedure btnDeleteSelectedClick(Sender: TObject);
    procedure vtVersionsCompareNodes(Sender: TBaseVirtualTree; Node1,
      Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
  private
    { Private declarations }
    FDefaultStyleName: string;

    FConnStr: string;
    procedure FillTree(Versions: TVersionList);
    procedure ReloadData;

    procedure intAddOrEditItemByNode(Node: PVirtualNode);
    procedure intAddOrEditItem(Item: TVersionControlItem);
    procedure intDeleteItemByNode(Node: PVirtualNode);
    procedure intDeleteItem(Item: TVersionControlItem);

    procedure LoadOptions;
    procedure SaveOptions;
  public
    { Public declarations }
  end;

var
  fmMain: TfmMain;

implementation

uses
  Vcl.Themes,
  Data.Win.ADODB,
  Xml.XMLIntf,
  Xml.XMLDoc,
  uXMLFunctions,
  uThreadSafeLogger,
  uVirtualTreeViewHelper,
  VersionControl.ItemEditorForm;

{$R *.dfm}

procedure TfmMain.btnAddNewClick(Sender: TObject);
begin
  intAddOrEditItemByNode(nil);
end;

procedure TfmMain.btnDBConnectionClick(Sender: TObject);
var
  s: string;
begin
  s := PromptDataSource(Handle, FConnStr);
  if s <> FConnStr then
  begin
    FConnStr := s;
    ReloadData;
    SaveOptions;
  end;
end;

procedure TfmMain.btnDeleteSelectedClick(Sender: TObject);
begin
  if Assigned(vtVersions.FocusedNode) then
    intDeleteItemByNode(vtVersions.FocusedNode);
end;

procedure TfmMain.btnEditSelectedClick(Sender: TObject);
begin
  if Assigned(vtVersions.FocusedNode) then
    intAddOrEditItemByNode(vtVersions.FocusedNode);
end;

procedure TfmMain.FillTree(Versions: TVersionList);
var
  i: Integer;
  Item: TVersionControlItem;
  b: Boolean;
begin
  vtVersions.BeginUpdate;
  try
    vtVersions.Clear;

    for i := 0 to Versions.Count - 1 do
    begin
      Item := Versions[i];
      vtVersions.InsertNodeWithObject(nil, amAddChildLast, Item);
    end;

    b := Versions.OwnsObjects;
    try
      Versions.OwnsObjects := False;
      Versions.Clear;
    finally
      Versions.OwnsObjects := b;
    end;

  finally
    vtVersions.EndUpdate;
  end;
end;

procedure TfmMain.FormCreate(Sender: TObject);
begin
  LoadOptions;
  if FConnStr <> '' then
    ReloadData;

  if FDefaultStyleName <> '' then
    TStyleManager.TrySetStyle(FDefaultStyleName);
  if Assigned(TStyleManager.ActiveStyle) then
    FDefaultStyleName := TStyleManager.ActiveStyle.Name;
end;

procedure TfmMain.FormDestroy(Sender: TObject);
begin
  SaveOptions;
end;

procedure TfmMain.intAddOrEditItem(Item: TVersionControlItem);
var
  ModuleName, ModuleMinDBVersion, ModuleActualVersion, Blacklist: string;
begin
  ModuleName := Item.ModuleName;
  ModuleMinDBVersion := Item.MinDBSupportedVersion.ToString;
  ModuleActualVersion := Item.ModuleActualVersion.ToString;
  Blacklist:=Item.VersionBlacklist.Text;
  if VersionControl.ItemEditorForm.AddOrEditItem(ModuleName, ModuleMinDBVersion, ModuleActualVersion, Blacklist) then
  begin
    Item.ModuleName := ModuleName;
    Item.MinDBSupportedVersion.FromString(ModuleMinDBVersion);
    Item.ModuleActualVersion.FromString(ModuleActualVersion);
    Item.VersionBlacklist.Text := Blacklist;
    try
      TVersionControl.WriteVersionItemToDB(FConnStr, Item);
    except
      on e: Exception do
      begin
        LogFileX.LogException(e);
        ShowMessage('Can`t write version info to DB');
      end;
    end;

    TThread.CreateAnonymousThread(
        procedure
      begin
        TThread.Queue(nil, ReloadData)
      end).Start;
  end;
end;

procedure TfmMain.intAddOrEditItemByNode(Node: PVirtualNode);
var
  Item: TVersionControlItem;
begin
  if Assigned(Node) then
  begin
    Item := vtVersions.GetObjectByNode<TVersionControlItem>(Node);
    intAddOrEditItem(Item);
  end
  else
  begin
    Item := TVersionControlItem.Create('', TVersion.Create(''));
    try
      intAddOrEditItem(Item);
    finally
      Item.Free;
    end;
  end;
end;

procedure TfmMain.intDeleteItem(Item: TVersionControlItem);
begin
  TVersionControl.DeleteItemFromDB(FConnStr, Item);

  TThread.CreateAnonymousThread(
    procedure
    begin
      TThread.Queue(nil, ReloadData)
    end).Start;
end;

procedure TfmMain.intDeleteItemByNode(Node: PVirtualNode);
var
  Item: TVersionControlItem;
begin
  if Assigned(Node) then
  begin
    Item := vtVersions.GetObjectByNode<TVersionControlItem>(Node);
    intDeleteItem(Item);
  end;
end;

procedure TfmMain.LoadOptions;
var
  Xml: IXMLDocument;
begin
  Xml := SafeLoadXMLDocument(ChangeFileExt(ParamStr(0), '.xml'));
  if Assigned(Xml) then
    if Assigned(Xml.DocumentElement) then
    begin
      FConnStr := GetNodeValueByName(Xml.DocumentElement.ChildNodes, 'Connection');
      FDefaultStyleName := GetNodeValueByName(Xml.DocumentElement.ChildNodes, 'Style');
    end;
end;

procedure TfmMain.pnlTopClick(Sender: TObject);
var
  SL: TStringList;
  Names: TArray<string>;
  i: Integer;
begin
  SL := TStringList.Create;
  try
    Names := TStyleManager.StyleNames;
    for i := low(Names) to high(Names) do
      SL.Add(Names[i]);

    if Assigned(TStyleManager.ActiveStyle) then
    begin
      i := SL.IndexOf(TStyleManager.ActiveStyle.Name);
      if i <> -1 then
        Inc(i);
      if i >= SL.Count then
        i := 0;
      if i < SL.Count then
      begin
        TStyleManager.TrySetStyle(SL[i]);
        FDefaultStyleName := SL[i];
        SaveOptions;
      end;
    end;
  finally
    SL.Free;
  end;
end;

procedure TfmMain.ReloadData;
var
  Versions: TVersionList;
begin
  try
    Versions := TVersionList.Create;
    try
      TVersionControl.GetAllDBVersions(FConnStr, Versions);
      FillTree(Versions);
    finally
      Versions.Free;
    end;
  except
    on e: Exception do
      ShowMessage('Cant load data. Please check connection string');
  end;
end;

procedure TfmMain.SaveOptions;
var
  Xml: IXMLDocument;
  RootNode: IXMLNode;
begin
  Xml := NewXMLDocument;
  RootNode := Xml.AddChild('VersionControl');
  RootNode.AddChild('Connection').Text := FConnStr;
  RootNode.AddChild('Style').Text := FDefaultStyleName;

  Xml.SaveToFile(ChangeFileExt(ParamStr(0), '.xml'));
end;

procedure TfmMain.vtVersionsCompareNodes(Sender: TBaseVirtualTree; Node1,
  Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
begin
  Result:=AnsiCompareText(vtVersions.Text[Node1, Column], vtVersions.Text[Node2,Column]);
end;

procedure TfmMain.vtVersionsFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  vtVersions.GetObjectByNode<TObject>(Node).Free;
end;

procedure TfmMain.vtVersionsGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
TextType: TVSTTextType; var CellText: string);
var
  Item: TVersionControlItem;
begin
  Item := vtVersions.GetObjectByNode<TVersionControlItem>(Node);
  if not Assigned(Item) then
    exit;

  case Column of
    0:
      CellText := Item.ModuleName;
    1:
      if Item.MinDBSupportedVersion.FullVer <> 0 then
        CellText := Item.MinDBSupportedVersion.ToString
      else
        CellText := 'Ignored';
    2:
      if Item.ModuleActualVersion.FullVer <> 0 then
        CellText := Item.ModuleActualVersion.ToString
      else
        CellText := '-';
    3:
      CellText := StringReplace(Item.VersionBlacklist.Text, '=', '-', [rfReplaceAll]);
  end;
end;

procedure TfmMain.vtVersionsNodeDblClick(Sender: TBaseVirtualTree; const HitInfo: THitInfo);
begin
  if hiOnItem in HitInfo.HitPositions then
    intAddOrEditItemByNode(HitInfo.HitNode);
end;

end.
