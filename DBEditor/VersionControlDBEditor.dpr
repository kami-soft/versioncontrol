program VersionControlDBEditor;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  VersionControl.MainDBEditorForm in 'VersionControl.MainDBEditorForm.pas' {fmMain},
  VersionControl.ItemEditorForm in 'VersionControl.ItemEditorForm.pas' {fmEdit},
  VersionControl.Classes in '..\VersionControl.Classes.pas',
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Amakrits');
  Application.CreateForm(TfmMain, fmMain);
  Application.Run;
end.
