unit VersionControl.ItemEditorForm;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TfmEdit = class(TForm)
    leModuleName: TLabeledEdit;
    leModuleMinDBVersion: TLabeledEdit;
    leModuleActualVersion: TLabeledEdit;
    btnOK: TButton;
    btnCancel: TButton;
    mmBlacklist: TMemo;
    lbVersionBlacklist: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Fill(const AModuleName, AModuleMinDBVersion, AModuleActualVersion, ABlacklist: string);
    procedure Apply(out AModuleName, AModuleMinDBVersion, AModuleActualVersion, ABlacklist: string);
  end;

function AddOrEditItem(var AModuleName, AModuleMinDBVersion, AModuleActualVersion, ABlacklist: string): Boolean;

implementation

uses
  System.UITypes;

{$R *.dfm}

function AddOrEditItem(var AModuleName, AModuleMinDBVersion, AModuleActualVersion, ABlacklist: string): Boolean;
var
  fm: TfmEdit;
begin
  fm := TfmEdit.Create(nil);
  try
    fm.Fill(AModuleName, AModuleMinDBVersion, AModuleActualVersion, ABlacklist);
    if IsPositiveResult(fm.ShowModal) then
    begin
      Result := True;
      fm.Apply(AModuleName, AModuleMinDBVersion, AModuleActualVersion, ABlacklist);
    end
    else
      Result := False;
  finally
    fm.Free;
  end;
end;

{ TfmEdit }

procedure TfmEdit.Apply(out AModuleName, AModuleMinDBVersion, AModuleActualVersion, ABlacklist: string);
var
  i: Integer;
begin
  AModuleName := leModuleName.Text;
  AModuleMinDBVersion := leModuleMinDBVersion.Text;
  AModuleActualVersion := leModuleActualVersion.Text;

  for i := mmBlacklist.Lines.Count - 1 downto 0 do
    if Trim(mmBlacklist.Lines[i]) = '' then
      mmBlacklist.Lines.Delete(i);
  ABlacklist := StringReplace(mmBlacklist.Text, ' ', '', [rfReplaceAll]);
  ABlacklist := StringReplace(ABlacklist, '-', '=', [rfReplaceAll]);
end;

procedure TfmEdit.Fill(const AModuleName, AModuleMinDBVersion, AModuleActualVersion, ABlacklist: string);
begin
  leModuleName.Text := AModuleName;
  leModuleMinDBVersion.Text := AModuleMinDBVersion;
  leModuleActualVersion.Text := AModuleActualVersion;
  mmBlacklist.Text := StringReplace(ABlacklist, '=', '-', [rfReplaceAll]);
end;

end.
