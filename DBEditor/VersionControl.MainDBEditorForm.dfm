object fmMain: TfmMain
  Left = 0
  Top = 0
  Caption = 'Version control DB editor'
  ClientHeight = 261
  ClientWidth = 802
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 340
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object vtVersions: TVirtualStringTree
    Left = 0
    Top = 41
    Width = 802
    Height = 220
    Align = alClient
    Header.AutoSizeIndex = 0
    Header.Font.Charset = DEFAULT_CHARSET
    Header.Font.Color = clWindowText
    Header.Font.Height = -11
    Header.Font.Name = 'Tahoma'
    Header.Font.Style = []
    Header.Height = 40
    Header.Options = [hoColumnResize, hoDrag, hoShowSortGlyphs, hoVisible, hoHeaderClickAutoSort]
    TabOrder = 0
    TreeOptions.AutoOptions = [toAutoDropExpand, toAutoScrollOnExpand, toAutoSort, toAutoTristateTracking, toAutoDeleteMovedNodes]
    TreeOptions.SelectionOptions = [toFullRowSelect, toRightClickSelect]
    TreeOptions.StringOptions = [toAutoAcceptEditChange]
    OnCompareNodes = vtVersionsCompareNodes
    OnFreeNode = vtVersionsFreeNode
    OnGetText = vtVersionsGetText
    OnNodeDblClick = vtVersionsNodeDblClick
    Columns = <
      item
        Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
        Position = 0
        Width = 180
        WideText = 'Module name'
      end
      item
        Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
        Position = 1
        Width = 120
        WideText = 'Min supported DB version'
      end
      item
        Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
        Position = 2
        Width = 120
        WideText = 'Actual version'
      end
      item
        Position = 3
        Width = 200
        WideText = 'Blacklist'
      end>
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 802
    Height = 41
    Align = alTop
    ShowCaption = False
    TabOrder = 1
    OnClick = pnlTopClick
    object btnAddNew: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 91
      Height = 33
      Align = alLeft
      Caption = 'Add new'
      TabOrder = 0
      OnClick = btnAddNewClick
    end
    object btnEditSelected: TButton
      AlignWithMargins = True
      Left = 101
      Top = 4
      Width = 91
      Height = 33
      Align = alLeft
      Caption = 'Edit selected'
      TabOrder = 1
      OnClick = btnEditSelectedClick
    end
    object btnDBConnection: TButton
      AlignWithMargins = True
      Left = 707
      Top = 4
      Width = 91
      Height = 33
      Align = alRight
      Caption = 'DB connection'
      TabOrder = 2
      OnClick = btnDBConnectionClick
    end
    object btnDeleteSelected: TButton
      AlignWithMargins = True
      Left = 198
      Top = 4
      Width = 91
      Height = 33
      Align = alLeft
      Caption = 'Delete selected'
      TabOrder = 3
      OnClick = btnDeleteSelectedClick
    end
  end
end
