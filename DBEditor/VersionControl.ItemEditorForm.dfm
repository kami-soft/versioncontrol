object fmEdit: TfmEdit
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Version control item'
  ClientHeight = 324
  ClientWidth = 262
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbVersionBlacklist: TLabel
    Left = 8
    Top = 156
    Width = 220
    Height = 13
    Caption = 'Blacklist ranges. Use "1.0.0.1-2.0.5.6" format'
  end
  object leModuleName: TLabeledEdit
    Left = 8
    Top = 24
    Width = 243
    Height = 21
    AutoSize = False
    EditLabel.Width = 63
    EditLabel.Height = 13
    EditLabel.Caption = 'Module name'
    TabOrder = 0
  end
  object leModuleMinDBVersion: TLabeledEdit
    Left = 8
    Top = 72
    Width = 243
    Height = 21
    AutoSize = False
    EditLabel.Width = 148
    EditLabel.Height = 13
    EditLabel.Caption = 'Minimal DB compatibled version'
    TabOrder = 1
  end
  object btnOK: TButton
    Left = 88
    Top = 291
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
  end
  object btnCancel: TButton
    Left = 176
    Top = 291
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object leModuleActualVersion: TLabeledEdit
    Left = 8
    Top = 120
    Width = 243
    Height = 21
    AutoSize = False
    EditLabel.Width = 68
    EditLabel.Height = 13
    EditLabel.Caption = 'Actual version'
    TabOrder = 2
  end
  object mmBlacklist: TMemo
    Left = 8
    Top = 172
    Width = 243
    Height = 101
    ScrollBars = ssVertical
    TabOrder = 5
  end
end
