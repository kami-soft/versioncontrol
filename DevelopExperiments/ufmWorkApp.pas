unit ufmWorkApp;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs;

type
  TForm7 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    h: THandle;
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

uses
  VersionControl.ItemListFrame,
  VersionControl.ManagerForm;

{$R *.dfm}

resourcestring
  rsWorkDll = 'VersionControlEx:WorkDLL.dll=0.3.5.6';
  rsWorkApp = 'VersionControlEx:WorkApp.exe=10.0.0.0';

const
  ConnStr ='Provider=SQLOLEDB.1;Password=pulkovo294;Persist Security Info=True;User ID=pulkovo;Initial Catalog=RDS;Data Source=srvr154';

procedure TForm7.FormCreate(Sender: TObject);
var
  fr: TfrVersionControl;
  s: string;
begin
  h:=LoadLibrary('WorkDll.dll');

  fr := TfrVersionControl.Create(nil);
  fr.parent := Self;
  fr.Align := alClient;

  TfmVersionManager.CheckCompatiblity(ConnStr, '123', True, 0);
  TfmVersionManager.PerformPeriodicallyChecking(ConnStr, '123', True, 10);

  fr.DisplayAllItems(ConnStr);

  s := rsWorkDll + rsWorkApp;
end;


procedure TForm7.FormDestroy(Sender: TObject);
begin
  FreeLibrary(h);
end;

end.
