program WorkApp;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  ufmWorkApp in 'ufmWorkApp.pas' {Form7},
  VersionControl.Classes in '..\VersionControl.Classes.pas',
  VersionControl.ItemListFrame in '..\VersionControl.ItemListFrame.pas' {frVersionControl: TFrame},
  VersionControl.ManagerForm in '..\VersionControl.ManagerForm.pas' {fmVersionManager};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm7, Form7);
  Application.Run;
end.
