library WorkDLL;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  System.SysUtils,
  System.Classes,
  VersionControl.Classes in '..\VersionControl.Classes.pas';

{$R *.res}

resourcestring
  SomeModuleMinimalSupportedVersion = 'VersionControlEx:twain_32.dll=1.3.9.6';
  sX = 'VersionControlEx:WorkDll.dll=3.3.9.6';
  rsWorkApp = 'VersionControlEx:WorkApp.exe=2.3.5.6';

var
  s: string;

begin
  s := SomeModuleMinimalSupportedVersion+sX+rsWorkApp;

end.
